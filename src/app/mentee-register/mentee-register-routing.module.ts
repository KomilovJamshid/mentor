import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenteeRegisterComponent } from './mentee-register.component';

const routes: Routes = [
  {
    path: '',
    component: MenteeRegisterComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenteeRegisterRoutingModule { }
