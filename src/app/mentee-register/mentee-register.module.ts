import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenteeRegisterRoutingModule } from './mentee-register-routing.module';
import { MenteeRegisterComponent } from './mentee-register.component';


@NgModule({
  declarations: [MenteeRegisterComponent],
  imports: [
    CommonModule,
    MenteeRegisterRoutingModule
  ]
})
export class MenteeRegisterModule { }
