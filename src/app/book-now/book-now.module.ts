import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookNowComponent } from './book-now.component';
import { BookNowRoutingModule } from './book-now-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Daterangepicker } from 'ng2-daterangepicker';

@NgModule({
  declarations: [BookNowComponent],
  imports: [CommonModule, BookNowRoutingModule, NgbModule, Daterangepicker],
})
export class BookNowModule {}
