import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenteeChatComponent } from './mentee-chat.component';

const routes: Routes = [
  {
    path : '',
    component : MenteeChatComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenteeChatRoutingModule { }
