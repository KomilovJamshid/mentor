import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenteeChatComponent } from './mentee-chat.component';

describe('MenteeChatComponent', () => {
  let component: MenteeChatComponent;
  let fixture: ComponentFixture<MenteeChatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenteeChatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenteeChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
