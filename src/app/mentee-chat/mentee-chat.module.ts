import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenteeChatRoutingModule } from './mentee-chat-routing.module';
import { MenteeChatComponent } from './mentee-chat.component';


@NgModule({
  declarations: [MenteeChatComponent],
  imports: [
    CommonModule,
    MenteeChatRoutingModule
  ]
})
export class MenteeChatModule { }
