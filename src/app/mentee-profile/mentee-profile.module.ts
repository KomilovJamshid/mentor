import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenteeProfileRoutingModule } from './mentee-profile-routing.module';
import { MenteeProfileComponent } from './mentee-profile.component';


@NgModule({
  declarations: [MenteeProfileComponent],
  imports: [
    CommonModule,
    MenteeProfileRoutingModule
  ]
})
export class MenteeProfileModule { }
